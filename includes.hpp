#include <stdio.h>
#include <string.h>

// Functions I made for easier printing - not part of the challenge!
#define printNumber(number) printFormat(number, "%d")
#define printString(string) printFormat(string, "%s")
#define printFunction(function) printFormat(function, "%p")
#define printFormat(s,f) for(int i=printf("[%p]   %s   |"f"|\n",&s, #s, s); --i?1:!putchar('\n');putchar('='))