#include "includes.hpp"

// Used to create a "function" type
typedef void(*function)();

// Call me!
void win()
{
    printf("YOU WON!\n");
}

// Don't call me!
void lose()
{
    printf("you lost..\n");
}

int main(void)
{
    function func = lose; // A variable that saves a function's memory address
    char string[] = "string";

    /*
    HINT:
    functions are memory locations. So if we know where a function is saved
    in memory we can execute (call) the function based on it's memory location!
    */

    printString(string);
    printFunction(func);
    printFunction(lose);
    printFunction(win);
    printf("\n");
    
    printf("\nEnter the string: ");
    scanf("%s", string);
    printf("\n\n");

    printString(string);
    printFunction(func);
    printFunction(lose);
    printFunction(win);
    printf("\n");

    printf("Calling func: ");
    func();

    return 0;
}
