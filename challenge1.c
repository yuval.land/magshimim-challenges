#include "includes.hpp"

int main(void)
{
    // Just 2 normal strigs... right?
    char secret[] = "Secret String";
    char string[] = "Normal String";

    // Here, see for yourselves!
    printString(string);
    printString(secret);
    printf("\n");
    
    // Setting the new value of string.
    printf("Enter the new string: ");
    scanf("%s", string);
    printf("\n\n");

    // Should stay the same, right?
    printString(string);
    printString(secret);
    printf("\n");
    
    // How can you change the secret without changing the string?
    if (!strcmp(secret, "password"))
        printf("YOU WON!\n");
    else
        printf("you lost...\n");

    return 0;
}
