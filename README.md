# Magshimim Challenges

A collection of challenges I made for Magshimim.
Will add more as time goes on.

<br>

***NOTE:*** compile with: `gcc -fno-stack-protector -z execstack -no-pie -o challenge1 challenge1.c` to be sure your compiler removes any safeguards it has.

<br>

## challenge1.c
### Buffer Overflow #1 - Introduction
Do you remember why we use "fgets" and not "gets" or "scanf"?

<br>

## challenge2.c
### Buffer Overflow #2 - Step it up
What makes a string different from a normal array of chars?

<br>

## challenge3.c
### Buffer Overflow #3 - R.O.P: Choosing which function to call
Have you heard of Endians?

<br>

## challenge4.c
### Format String Vulnerability - Why % matters
A problem with `printf`? No way!