#include "includes.hpp"

int main(void)
{
    char string[50] = "string";
    int number = 10;
    
    printString(string);
    printNumber(number);

    // No overflow this time...    
    printf("\nEnter the string: ");
    fgets(string, 50, stdin);
    string[strcspn(string, "\n")] = 0; // Removes the new line at the end
    printf("\n");
    
    printString(string);
    printNumber(number);

    // Similar to "scanf", what does '&' do?
    printf("The string you entered is: ");
    printf(string);
    printf("\nnumber: %d\n", number);

    if (number == 20)
        printf("YOU WON!\n");
    else
        printf("you lost...\n");

    return 0;
}