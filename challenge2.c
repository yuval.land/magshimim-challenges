#include "includes.hpp"

int main(void)
{
    char s3[] = "Change me!";
    char s2[] = "unchangeable";
    char s1[] = "string";

    printString(s1);
    printString(s2);
    printString(s3);
    printf("\n");

    printf("Enter the new value for s1: ");
    scanf("%s", s1);
    printf("\n\n");
    
    printString(s1);
    printString(s2);
    printString(s3);
    printf("\n");

    if (strcmp(s2, "unchangeable"))
        printf("You cannot change s2!\n");
    else if (!strcmp(s3, "password"))
        printf("YOU WON!\n");
    else
        printf("you lost...\n");

    return 0;
}